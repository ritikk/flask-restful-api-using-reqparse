from flask import Flask
from flask_restful import Resource, Api, reqparse, abort

app = Flask(__name__)
api = Api(app)

todos = {
    1: {"task": "task1", "summary": "summary1"},
    2: {"task": "task2", "summary": "summary2"},
    3: {"task": "task3", "summary": "summary3"}
}

task_post_args = reqparse.RequestParser()
task_post_args.add_argument("task", type=str, help="Task is required", required=True)
task_post_args.add_argument("summary", type=str, help="summary is required", required=True)

task_put_args = reqparse.RequestParser()
task_put_args.add_argument("task", type=str)
task_put_args.add_argument("summary", type=str)


class Todo(Resource):
    def get(self, todo_id):
        return todos[todo_id]

    def post(self, todo_id):
        args = task_post_args.parse_args()
        if todo_id in todos:
            abort(409, "Task id already exists")
        todos[todo_id] = {"task": args["task"], "summary": args["summary"]}
        return todos[todo_id]

    def delete(self, todo_id):
        del todos[todo_id]
        return todos

    def put(self, todo_id):
        args = task_put_args.parse_args()
        if todo_id not in todos:
            abort(404, message="Task not exist")
        if args["task"]:
            todos[todo_id]["task"] = args["task"]
        if args["summary"]:
            todos[todo_id]["summary"] = args["summary"]
        return todos[todo_id]


class TodoList(Resource):
    def get(self):
        return todos


api.add_resource(Todo, "/todos/<int:todo_id>")
api.add_resource(TodoList, "/todos")

if __name__ == "__main__":
    app.run(debug=True)
